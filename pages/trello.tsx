import type { NextPage } from 'next'

import styles from '../styles/Scroll.module.css'

const Trello: NextPage = () => {
  return (
    <div className={`bg-sky-600 h-screen overflow-y-hidden ${styles.scroll}` }>
      <header className='bg-sky-800 flex justify-between items-center h-10 w-full sticky left-0'>
      <div className='flex py-2'>
        <div className='w-24 h-7 bg-sky-700 mx-2 rounded-sm' />
        <div className='w-44 h-7 bg-sky-700 rounded-sm' />
      </div>
      <div className="py-2 bg-sky-700 w-24 h-7 rounded-sm"/>
      <div className='flex py-2'>
        <div className='w-7 h-7 bg-sky-700  rounded-sm' />
        <div className='w-7 h-7 bg-sky-700 ml-2 rounded-sm' />
        <div className='w-7 h-7 bg-sky-700 ml-2 rounded-sm' />
        <div className='w-7 h-7 bg-sky-700 mx-2 rounded-sm' />
      </div>
    </header>
    <div className='mt-2 mb-3 flex items-center h-4'>
      <div className="w-16 h-4 ml-2 bg-white rounded-md"/>
      <div className="w-24 h-4 ml-2 bg-white rounded-md"/>
      <div className='w-4 h-4 bg-white ml-6 rounded-md' />
      <div className='w-4 h-4 bg-white ml-2 rounded-md' />
      <div className="w-16 h-4 ml-2 bg-white rounded-md"/>
    </div>
    <div className="flex items-start px-2">
      <div className="bg-gray-200 rounded-md p-2 ml-2 h-fit">
        <div className="w-20 h-4 mb-2 bg-gray-400 rounded-md"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-40 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-20 h-4 my-4 bg-white rounded-md"/>
      </div>
      <div className="bg-gray-200 rounded-md p-2 ml-2 h-fit">
        <div className="w-20 h-4 mb-2 bg-gray-400 rounded-md"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-14 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-20 h-4 my-4 bg-white rounded-md"/>
      </div>
      <div className="bg-gray-200 rounded-md p-2 ml-2 h-fit">
        <div className="w-20 h-4 mb-2 bg-gray-400 rounded-md"/>
        <div className="w-64 h-40 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-14 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-20 h-4 my-4 bg-white rounded-md"/>
      </div>
      <div className="bg-gray-200 rounded-md p-2 ml-2 h-fit">
        <div className="w-20 h-4 mb-2 bg-gray-400 rounded-md"/>
        <div className="w-64 h-40 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-14 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-20 h-4 my-4 bg-white rounded-md"/>
      </div>
      <div className="bg-gray-200 rounded-md p-2 ml-2 h-fit">
        <div className="w-20 h-4 mb-2 bg-gray-400 rounded-md"/>
        <div className="w-64 h-40 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-14 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-20 h-4 my-4 bg-white rounded-md"/>
      </div>
      <div className="bg-gray-200 rounded-md p-2 ml-2 h-fit">
        <div className="w-20 h-4 mb-2 bg-gray-400 rounded-md"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-14 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-20 h-4 my-4 bg-white rounded-md"/>
      </div>
      <div className="bg-gray-200 rounded-md p-2 ml-2 h-fit">
        <div className="w-20 h-4 mb-2 bg-gray-400 rounded-md"/>
        <div className="w-64 h-40 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-64 h-14 bg-white rounded-md mt-2"/>
        <div className="w-64 h-20 bg-white rounded-md mt-2"/>
        <div className="w-20 h-4 my-4 bg-white rounded-md"/>
      </div>
    </div>
    </div>
  )
}

export default Trello