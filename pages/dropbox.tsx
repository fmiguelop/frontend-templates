import type { NextPage } from 'next';

const Dropbox: NextPage = () => (
  <div className="flex">
    <div className="w-2/12 h-screen bg-gray-200 flex flex-col justify-between">
      <div className="mt-9 ml-10">
        <div className="w-10 h-10 bg-blue-600 rounded"/>
        <div className="mt-8 w-10 h-4 bg-blue-600 rounded"/>
        <div className="mt-11 w-14 h-4 bg-blue-600 rounded"/>
        <div className="mt-4 w-12 h-4 bg-gray-400 rounded"/>
        <div className="mt-4 w-24 h-4 bg-gray-400 rounded"/>
        <div className="mt-4 w-20 h-4 bg-gray-400 rounded"/>
      </div>
      <div className="mx-4 border-t border-gray-400 h-24">
        <div className="mt-6 flex items-start justify-between">
          <div className=''>
            <div className="w-24 h-4 bg-gray-400 rounded"/>
            <div className="w-20 h-4 bg-gray-400 rounded mt-2"/>
          </div>
          <div className="w-6 h-6 bg-gray-400 rounded" />
        </div>
      </div>
    </div>
    <div className="w-10/12 px-10">
      <header className='h-16 mt-3 mb-8  flex justify-between items-end'>
        <div className="w-20 h-6 bg-gray-300 rounded"/>
        <div className="h-full">
          <div className="flex flex-col items-end">
            <div className='flex items-center'>
              <div className="w-6 h-6 bg-gray-200 rounded"/>
              <div className="w-32 h-4 bg-gray-200 rounded ml-2"/>
            </div>
            <div className="flex items-center mt-2">
              <div className='w-56 h-8 rounded-full border-2 border-gray-300'/>
              <div className='w-6 h-6 bg-gray-200 mx-5'/>
              <div className='w-8 h-8 bg-yellow-500 rounded-full'/>
            </div>
          </div>
        </div>
      </header>
      <div className='w-full flex'>
        <div className="w-10/12">
          <table className='table-fixed w-11/12'>
            <thead className='border-b border-gray-300'>
              <tr>
                <th>
                  <div className="w-16 h-4 bg-gray-300 rounded  mb-3"/>
                </th>
                <th>
                  <div className="w-16 h-4 bg-gray-300 rounded  mb-3"/>
                </th>
                <th>
                  <div className="w-16 h-4 bg-gray-300 rounded  mb-3"/>
                </th>
                <th>
                  <div className="w-4 h-4 bg-gray-300 rounded mb-3 ml-10"/>
                </th>
              </tr>
            </thead>
            <tbody className=''>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-16 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-24 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-24 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-32 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-20 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-24 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-40 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-10 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-20 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-14 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-28 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-24 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
              <tr className='border-b '>
                <td>
                  <div className='flex items-center my-3'>
                    <div className='bg-blue-200 w-8 h-8 mr-4 rounded'/>
                    <div className="bg-gray-400 h-4 w-32 rounded" />
                  </div>
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-4 w-16 rounded" />
                </td>
                <td>
                  <div className="bg-gray-200 h-8 w-8 rounded" />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="w-2/12 ml-10 flex-col flex items-center justify-between">
          <div>
            <div className="w-full h-8 bg-blue-600 rounded"/>
            <div className='ml-4'>
              <div className="flex items-center mt-3">
                <div className="w-6 h-6 bg-gray-200 mr-4 rounded"/>
                <div className="w-36 h-4 bg-gray-200 rounded"/>
              </div>
              <div className="flex items-center mt-3">
                <div className="w-6 h-6 bg-gray-200 mr-4 rounded"/>
                <div className="w-28 h-4 bg-gray-200 rounded"/>
              </div>
              <div className="flex items-center mt-3">
                <div className="w-6 h-6 bg-gray-200 mr-4 rounded"/>
                <div className="w-32 h-4 bg-gray-200 rounded"/>
              </div>
            </div>
          </div>
          <div className='w-48 h-60 border-2 border-gray-400' />
        </div>
      </div>
    </div>
  </div>
)

export default Dropbox;
