import type { NextPage } from 'next'

const Pinterest: NextPage = () => (
  <div>
    <header className='px-8 py-7 flex items-center'>
      <div className="w-8 h-8 bg-red-600 rounded-full"/>
      <div className="w-11/12 h-10 border border-gray-300 rounded mx-8"/>
      <div className="w-8 h-8 bg-gray-200 rounded"/>
      <div className="w-8 h-8 bg-gray-200 rounded mx-6"/>
      <div className="w-8 h-8 bg-gray-200 rounded"/>
    </header>
    <nav className='mt-6 mb-10 flex justify-center'>
      <div className="w-2/5 h-14 bg-gray-200 rounded">
        <div className="w-1/5 h-12 my-1 ml-1 bg-white rounded"/>
      </div> 
    </nav>
    <div className="px-20 mb-2 grid grid-cols-5 gap-x-6">
      <div>
        <div className='mb-10'>
          <div className="w-full bg-green-600 rounded h-96"/>
          <div className="mt-2 w-full rounded bg-gray-400 h-9"/>
          <div className="my-1 w-full rounded bg-gray-200 h-14"/>
          <div className="flex">
            <div className="w-6 h-6 rounded-full bg-yellow-600 mr-1.5"/>
            <div className="w-full h-6 rounded bg-gray-200"/>
          </div>
        </div>
        <div>
          <div className="w-full bg-amber-600 rounded h-[25rem]"/>
          <div className="mt-2 w-full rounded bg-gray-400 h-9"/>
          <div className="my-1 w-full rounded bg-gray-200 h-14"/>
          <div className="flex">
            <div className="w-6 h-6 rounded-full bg-yellow-600 mr-1.5"/>
            <div className="w-full h-6 rounded bg-gray-200"/>
          </div>
        </div>
      </div>
      <div>
        <div className='mb-10'>
          <div className="w-full bg-cyan-600 rounded h-[35rem]"/>
          <div className="mt-2 w-full rounded bg-gray-400 h-9"/>
          <div className="my-1 w-full rounded bg-gray-200 h-14"/>
          <div className="flex">
            <div className="w-6 h-6 rounded-full bg-yellow-600 mr-1.5"/>
            <div className="w-full h-6 rounded bg-gray-200"/>
          </div>
        </div>
        <div>
          <div className="w-full bg-stone-600 rounded h-56"/>
          <div className="mt-2 w-full rounded bg-gray-400 h-9"/>
          <div className="my-1 w-full rounded bg-gray-200 h-14"/>
          <div className="flex">
            <div className="w-6 h-6 rounded-full bg-yellow-600 mr-1.5"/>
            <div className="w-full h-6 rounded bg-gray-200"/>
          </div>
        </div>
      </div>
      <div>
        <div className='mb-10'>
          <div className="w-full bg-sky-600 rounded h-[34rem]"/>
          <div className="mt-2 w-full rounded bg-gray-400 h-9"/>
          <div className="my-1 w-full rounded bg-gray-200 h-14"/>
          <div className="flex">
            <div className="w-6 h-6 rounded-full bg-yellow-600 mr-1.5"/>
            <div className="w-full h-6 rounded bg-gray-200"/>
          </div>
        </div>
        <div>
          <div className="w-full bg-purple-600 rounded h-[15rem]"/>
          <div className="mt-2 w-full rounded bg-gray-400 h-9"/>
          <div className="my-1 w-full rounded bg-gray-200 h-14"/>
          <div className="flex">
            <div className="w-6 h-6 rounded-full bg-yellow-600 mr-1.5"/>
            <div className="w-full h-6 rounded bg-gray-200"/>
          </div>
        </div>
      </div>
      <div>
        <div className='mb-10'>
          <div className="w-full bg-blue-600 rounded h-96"/>
          <div className="mt-2 w-full rounded bg-gray-400 h-9"/>
          <div className="my-1 w-full rounded bg-gray-200 h-14"/>
          <div className="flex">
            <div className="w-6 h-6 rounded-full bg-yellow-600 mr-1.5"/>
            <div className="w-full h-6 rounded bg-gray-200"/>
          </div>
        </div>
        <div>
          <div className="w-full bg-rose-600 rounded h-[25rem]"/>
          <div className="mt-2 w-full rounded bg-gray-400 h-9"/>
          <div className="my-1 w-full rounded bg-gray-200 h-14"/>
          <div className="flex">
            <div className="w-6 h-6 rounded-full bg-yellow-600 mr-1.5"/>
            <div className="w-full h-6 rounded bg-gray-200"/>
          </div>
        </div>
      </div>
      <div>
        <div className='mb-10'>
          <div className="w-full bg-teal-600 rounded h-[40rem]"/>
          <div className="mt-2 w-full rounded bg-gray-400 h-9"/>
          <div className="my-1 w-full rounded bg-gray-200 h-14"/>
          <div className="flex">
            <div className="w-6 h-6 rounded-full bg-yellow-600 mr-1.5"/>
            <div className="w-full h-6 rounded bg-gray-200"/>
          </div>
        </div>
        <div>
          <div className="w-full bg-orange-600 rounded h-[9rem]"/>
          <div className="mt-2 w-full rounded bg-gray-400 h-9"/>
          <div className="my-1 w-full rounded bg-gray-200 h-14"/>
          <div className="flex">
            <div className="w-6 h-6 rounded-full bg-yellow-600 mr-1.5"/>
            <div className="w-full h-6 rounded bg-gray-200"/>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Pinterest;