import type { NextPage } from 'next';

const Facebook: NextPage = () => (
  <div className="">
    <header className='w-full h-14 flex justify-around bg-blue-900 opacity-95'>
      <div className="inline-flex items-center">
        <div className="w-6 h-6 bg-white mr-2 rounded-sm"/>
        <div className="w-[28rem] h-6 bg-white rounded-sm"/>
      </div>
      <div className='inline-flex items-center'>
        <div className='w-6 h-6 bg-red-600 rounded-sm'/>
        <div className="w-16 h-4 bg-gray-300 rounded-sm ml-2"/>
        <div className="w-16 h-4 bg-gray-300 rounded-sm ml-2"/>
        <div className="w-6 h-4 bg-gray-300 rounded-sm ml-4"/>
        <div className="w-6 h-4 bg-gray-300 rounded-sm ml-4"/>
        <div className="w-6 h-4 bg-gray-300 rounded-sm ml-4"/>
      </div>
    </header>
    <div className='w-full flex flex-row-reverse'>
      <div className="w-1/12 bg-gray-300 sticky top-0"/>
      <div className="w-11/12 bg-gray-100 flex items-start justify-center py-4">
        <div className='w-2/12'>
          <div className="w-full h-8 bg-gray-300 rounded-sm"/>
          <div className="w-full h-16 bg-gray-300 rounded-sm mt-2"/>
          <div className="w-full h-8 bg-gray-300 rounded-sm my-8"/>
          <div className="w-full h-56 bg-gray-300 rounded-sm mt-2"/>
        </div>
        <div className='mx-4 w-5/12'>
          <div className="w-full h-40 bg-white rounded-sm"/>
          <div className="w-full h-56 bg-white rounded-sm my-4"/>
          <div className="w-full h-96 bg-white rounded-sm my-4"/>
          <div className="w-full h-56 bg-white rounded-sm my-4"/>
        </div>
        <div className="w-3/12">
          <div className="w-full h-9 bg-white rounded-sm"/>
          <div className="w-full h-[32rem] bg-white rounded-sm my-4"/>
          <div className="w-full h-14 bg-white rounded-sm my-4"/>

        </div>
      </div>
    </div>
  </div>
)

export default Facebook;