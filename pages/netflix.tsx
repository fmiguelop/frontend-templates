import type { NextPage } from 'next';

const NetFlix: NextPage = () => (
  <div className='overflow-hidden'>
    <header className="flex justify-between bg-black px-16 py-5 w-full">
      <div className="flex items-center">
        <div className="bg-red-600 w-28 h-8 rounded"/>
        <div className="bg-red-600 w-20 h-4 rounded ml-14"/>
        <div className="bg-white w-8 h-4 rounded ml-10"/>
        <div className="bg-white w-8 h-4 rounded ml-10"/>
      </div>
      <div className="flex items-center">
        <div className="bg-white w-20 h-4 rounded"/>
        <div className="h-6 w-6 bg-white rounded mx-6" />
        <div className="h-6 w-6 bg-yellow-500 rounded mr-6" />
        <div className="bg-white w-20 h-4 rounded"/>
      </div>
    </header>
    <div className="bg-slate-700 py-56">
      <div className="w-1/3 ml-16">
        <div className="w-2/3 h-12 bg-gray-200 rounded" />
        <div className="w-full h-3 bg-gray-200 rounded mt-4" />
        <div className="w-full h-3 bg-gray-200 rounded mt-2" />
        <div className="w-1/3 h-3 bg-gray-200 rounded mt-2" />
        <div className="flex mt-8">
          <div className="w-24 h-9 bg-red-600 mr-2 rounded" />
          <div className="w-28 h-9 bg-black ml-2 rounded" />
        </div>
      </div>
    </div>
    <div className="bg-black px-16 h-fit">
      <div className='bottom-20 relative'>
        <div>
          <div className="w-72 h-6 bg-gray-300 mb-2" />
          <div className="flex flex-nowrap">
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-sky-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-pink-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-purple-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-blue-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-rose-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-green-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-sky-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
          </div>
        </div>
        <div className='mt-10'>
          <div className="w-72 h-6 bg-gray-300 mb-2" />
          <div className="flex flex-nowrap">
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-emerald-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-zinc-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-lime-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-teal-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-cyan-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-fuchsia-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-indigo-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
          </div>
        </div>
        <div className='mt-10'>
          <div className="w-72 h-6 bg-gray-300 mb-2" />
          <div className="flex flex-nowrap">
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-yellow-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-red-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-orange-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-amber-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-fuchsia-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-gray-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
            <div className="w-64 mr-2">
              <div className="w-full h-36 bg-emerald-400" />
              <div className="w-36 h-1 bg-white mt-2 mx-16 rounded" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default NetFlix;