import type { NextPage } from 'next'
import Image from 'next/image'
import Link from 'next/link'

const Home: NextPage = () => {
  return (
    <div className="bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 h-screen">
      <h1 className='text-6xl p-12 text-center font-semibold text-gray-100 tracking-widest'>Frontend templates</h1>
      <div className="grid grid-cols-4 gap-4 content-between justify-items-center">
        <Link href="/youtube">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-red-600 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-red-600 hover:shadow-red-600'>
            <h1 className='text-6xl font-bold'>Youtube</h1>
          </a>
        </Link>
        <Link href="/facebook">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-blue-600 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-blue-600 hover:shadow-blue-600'>
            <h1 className='text-6xl font-bold'>Facebook</h1>
          </a>
        </Link>
        <Link href="/netflix">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-red-600 hover:-translate-y-1 m-4 hover:shadow-2xl  hover:bg-black hover:shadow-black'>
            <h1 className='text-6xl font-bold'>Netflix</h1>
          </a>
        </Link>
        <Link href="/evernote">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-green-600 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-green-600 hover:shadow-green-600'>
            <h1 className='text-6xl font-bold'>Evernote</h1>
          </a>
        </Link>
        <Link href="/pinterest">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-red-400 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-red-400 hover:shadow-red-400'>
            <h1 className='text-6xl font-bold'>Pinterest</h1>
          </a>
        </Link>
        <Link href="/analytics">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-orange-400 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-orange-400 hover:shadow-orange-400'>
            <h1 className='text-5xl font-bold text-center'>Google Analytics</h1>
          </a>
        </Link>
        <Link href="/slack">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-sky-600 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-sky-600 hover:shadow-sky-600'>
            <h1 className='text-6xl font-bold'>Slack</h1>
          </a>
        </Link>
        <Link href="/mint">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-lime-600 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-lime-600 hover:shadow-lime-600'>
            <h1 className='text-6xl font-bold'>Mint</h1>
          </a>
        </Link>
        <Link href="/flights">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-sky-700 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-sky-700 hover:shadow-sky-700'>
            <h1 className='text-5xl font-bold text-center'>Google Flights</h1>
          </a>
        </Link>
        <Link href="/airbnb">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-pink-600 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-pink-600 hover:shadow-pink-600'>
            <h1 className='text-6xl font-bold'>Airbnb</h1>
          </a>
        </Link>
        <Link href="/dropbox">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-blue-600 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-blue-600 hover:shadow-blue-600'>
            <h1 className='text-6xl font-bold'>Dropbox</h1>
          </a>
        </Link>
        <Link href="/trello">
          <a className='rounded-lg shadow-2xl bg-white flex justify-center items-center h-44 w-72 transition duration-500 ease-in-out transform text-indigo-400 hover:-translate-y-1 m-4 hover:shadow-2xl hover:text-white hover:bg-indigo-400 hover:shadow-indigo-400'>
            <h1 className='text-6xl font-bold'>Trello</h1>
          </a>
        </Link>
      </div>
    </div>
  )
}

export default Home
