import type { NextPage } from 'next'

const Analytics: NextPage = () => {
  return (
    <div className='bg-gray-200'>
      <header className="w-full bg-white h-16 flex items-center justify-between">
        <div className="flex">
          <div className="w-8 h-8 mx-3 bg-yellow-400 rounded-sm"/>
          <div>
            <div className="h-3 w-28 bg-gray-200 rounded-sm"/>
            <div className="h-4 w-44 mt-1 bg-gray-400 rounded-sm"/>
          </div>
        </div>
        <div className='inline-flex items-center'>
        <div className='w-8 h-8 bg-gray-400 ml-72 rounded-sm'/>
        <div className='w-8 h-8 bg-gray-400 ml-2 rounded-sm'/>
        <div className='w-8 h-8 bg-gray-400 ml-2 rounded-sm'/>
        <div className='w-8 h-8 bg-violet-800 mx-2 rounded-full'/>
      </div>
      </header>
      <div className='flex'>
        <aside className='sticky top-0 h-screen'>
          <div className='w-64'>
            <div className='w-full flex flex-col border border-r-gray-400 bg-white justify-between h-[50rem]'>
              <div>
                <div className="flex items-center h-14 border-b-gray-400 border">
                <div className="w-6 h-6 bg-gray-200 mx-4 rounded-sm"/>
                <div className="w-36 h-4 bg-gray-200 rounded-sm"/>
              </div>
              <div className="flex items-center h-14 border-b-gray-400 border">
                <div className="w-6 h-6 bg-orange-400 mx-4 rounded-sm"/>
                <div className="w-16 h-4 bg-orange-400 rounded-sm"/>
              </div>
              <div className="flex items-center h-14 border-b-gray-400 border">
                <div className="w-6 h-6 bg-gray-200 mx-4 rounded-sm"/>
                <div className="w-36 h-4 bg-gray-200 rounded-sm"/>
              </div>
              <div className="flex items-center h-14 ">
                <div className="w-6 h-6 bg-gray-200 mx-4 rounded-sm"/>
                <div className="w-24 h-4 bg-gray-200 rounded-sm"/>
              </div>
              <div className="flex items-center h-14 ">
                <div className="w-6 h-6 bg-gray-200 mx-4 rounded-sm"/>
                <div className="w-24 h-4 bg-gray-200 rounded-sm"/>
              </div>
              <div className="flex items-center h-14 ">
                <div className="w-6 h-6 bg-gray-200 mx-4 rounded-sm"/>
                <div className="w-32 h-4 bg-gray-200 rounded-sm"/>
              </div>
              <div className="flex items-center h-14 ">
                <div className="w-6 h-6 bg-gray-200 mx-4 rounded-sm"/>
                <div className="w-24 h-4 bg-gray-200 rounded-sm"/>
              </div>
              <div className="flex items-center h-14 ">
                <div className="w-6 h-6 bg-gray-200 mx-4 rounded-sm"/>
                <div className="w-32 h-4 bg-gray-200 rounded-sm"/>
              </div>
              </div>
              <div>
                <div className="flex items-center h-14 border-b-gray-400 border">
                  <div className="w-6 h-6 bg-gray-200 mx-4 rounded-sm"/>
                  <div className="w-32 h-4 bg-gray-200 rounded-sm"/>
              </div>
                <div className="flex items-center h-14 border-b-gray-400 border">
                  <div className="w-6 h-6 bg-gray-200 mx-4 rounded-sm"/>
                  <div className="w-32 h-4 bg-gray-200 rounded-sm"/>
              </div>
              </div>
            </div>
          </div>
        </aside>
        <div className='w-full flex flex-col justify-center items-center'>
          <div className="m-4">
            <div className="w-48 h-6 bg-gray-400 mb-4"/>
            <div className="w-[63rem] h-[31rem] inline-flex">
              <div className="w-3/5 h-full bg-white"/>
              <div className="w-2/5 h-full bg-blue-500 ml-4"/>

            </div>
          </div>
          <div className="m-4">
            <div className="w-48 h-6 bg-gray-400 mb-4"/>
            <div className="w-[63rem] h-[31rem] bg-white"/>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Analytics