import type { NextPage } from 'next';

const Slack: NextPage = () => (
  <div className="flex">
    <div className="bg-stone-800 pt-1 w-20 h-screen">
      <div className="flex justify-evenly px-2">
        <div className="bg-red-400 rounded-full w-3 h-3" />
        <div className="bg-yellow-400 rounded-full w-3 h-3" />
        <div className="bg-green-400 rounded-full w-3 h-3" />
      </div>
      <div className="w-10 h-10 bg-red-400 rounded-md mx-4 mt-5" />
      <div className='flex mt-5'>
        <div className="h-10 w-1 bg-gray-200 rounded-r-lg"></div>
        <div className="w-10 h-10 bg-blue-400 mx-3 rounded-md" />
      </div>
      <div className="w-10 h-10 bg-yellow-400 rounded-md mx-4 mt-5" />
    </div>
    <div className="w-2/12 h-screen bg-slack-purple p-6 flex flex-col justify-between">
      <div>
        <div className="bg-white w-full h-6 rounded" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-44" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-20" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-36" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-28" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-52" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-12" />
        <div className="mt-4 bg-green-400 opacity-50 h-3 rounded w-3/5" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-32" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-32" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-32" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-3/5" />
        <div className="mt-10 bg-white opacity-50 h-3 rounded w-4/5" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-28" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-52" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-36" />
        <div className="mt-4 bg-white opacity-50 h-3 rounded w-20" />
      </div>
      <div className="w-full flex justify-center items-center">
        <div className="bg-white opacity-50 w-8 h-8 rounded"/>
      </div>
    </div>
    <div className="w-10/12 h-screen">
      <header className="border-b border-b-gray-200 w-full px-6 pt-6 pb-4 flex justify-between">
        <div className="">
          <div className="w-52 h-5 mb-2 rounded bg-gray-600" />
          <div className="w-80 h-3 mb-2 rounded bg-gray-300" />
        </div>
        <div className="flex items-center">
          <div className="w-6 h-6 bg-gray-200 rounded"/>
          <div className="w-6 h-6 bg-gray-200 rounded mx-4"/>
          <div className="w-6 h-6 bg-gray-200 rounded"/>
          <div className="w-52 h-10 bg-white border border-gray-300 rounded mx-4"/>
          <div className="w-6 h-6 bg-gray-200 rounded"/>
          <div className="w-6 h-6 bg-gray-200 rounded mx-4"/>
          <div className="w-6 h-6 bg-gray-200 rounded"/>
          <div className="w-10 h-10 bg-rose-500 rounded ml-4"/>
        </div>
      </header>
      <div className="w-full h-auto p-6">
        <div className="">
          <div className="flex items-start mb-4">
            <div className="w-10 h-10 bg-blue-500 rounded mr-4"/>
            <div className="w-full">
              <div className="bg-gray-700 rounded h-5 w-24"/>
              <div className="bg-gray-300 rounded w-2/5 h-3 my-4"/>
              <div className="bg-gray-300 rounded w-1/5 h-3"/>
            </div>
          </div>
          <div className="flex items-start mb-4">
            <div className="w-10 h-10 bg-purple-500 rounded mr-4"/>
            <div className="w-full">
              <div className="bg-gray-700 rounded h-5 w-24"/>
              <div className="bg-gray-300 rounded w-2/5 h-3 my-4"/>
              <div className="bg-gray-300 rounded w-1/5 h-3"/>
            </div>
          </div>
          <div className="flex items-start mb-4">
            <div className="w-10 h-10 bg-orange-500 rounded mr-4"/>
            <div className="w-full">
              <div className="bg-gray-700 rounded h-5 w-24"/>
              <div className="bg-gray-300 rounded w-2/5 h-3 my-4"/>
              <div className="bg-gray-300 rounded w-1/5 h-3"/>
            </div>
          </div>
        </div>
        <div className="w-3/4 px-4 py-3 rounded border border-gray-300 flex justify-between bottom-4 right-12 absolute">
          <div className="w-6 h-6 bg-gray-300 rounded" />
          <div className="w-6 h-6 bg-gray-300 rounded" />
        </div>
      </div>
    </div>
  </div>
)

export default Slack;