import type { NextPage } from 'next'

const Mint: NextPage = () => (
  <div>
    <header className='w-full flex justify-around items-center py-3 bg-stone-800'>
      <div className="w-24 h-10 bg-green-400 rounded"/>
      <div className="flex">
        <div className="w-24 h-4 bg-gray-400 rounded"/>
        <div className="w-0.5 h-4 bg-gray-400 rounded mx-4"/>
        <div className="w-12 h-4 bg-gray-400 rounded"/>
        <div className="w-0.5 h-4 bg-gray-400 rounded mx-4"/>
        <div className="w-10 h-4 bg-gray-400 rounded"/>
        <div className="w-0.5 h-4 bg-gray-400 rounded mx-4"/>
        <div className="w-10 h-4 bg-gray-400 rounded"/>
        <div className="w-0.5 h-4 bg-gray-400 rounded mx-4"/>
        <div className="w-12 h-4 bg-gray-400 rounded"/>
      </div>
    </header>
    <nav className='flex items-center justify-evenly px-60 bg-stone-100'>
      <div className='w-16 h-[4.5rem] border-b-4 border-b-green-500'>
        <div className="bg-green-500 h-4 w-full my-7 rounded"/>
      </div>
      <div className="bg-gray-300 w-24 h-4 rounded"/>
      <div className="bg-gray-300 w-8 h-4 rounded"/>
      <div className="bg-gray-300 w-14 h-4 rounded"/>
      <div className="bg-gray-300 w-12 h-4 rounded"/>
      <div className="bg-gray-300 w-12 h-4 rounded"/>
      <div className="bg-gray-300 w-24 h-4 rounded"/>
      <div className="bg-gray-300 w-24 h-4 rounded"/>
    </nav>
    <div className="w-full flex justify-center mt-8">
      <div className="border border-stone-300 w-3/12 h-fit sticky top-4 px-6 py-7">
        <div className="w-full border-b border-b-stone-300 flex justify-between">
          <div className="h-4 bg-black w-20 rounded"/>
          <div className="h-6 bg-gray-300 w-6 mb-4 rounded"/>
        </div>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
        <div className="mt-6 w-full bg-gray-300 h-12 rounded"/>
      </div>
      <div className="w-5/12 ml-6">
        <div className="border border-stone-300 mb-6 px-6 py-7">
          <div className="w-full border-b border-b-stone-300 flex justify-between">
            <div className="h-4 bg-black w-20 rounded"/>
            <div className="h-6 bg-gray-300 w-6 mb-4 rounded"/>
          </div>
          <div className="mt-8 flex w-full">
            <div className="w-2 h-2 rounded-full border border-yellow-500 mr-6"/>
            <div className="">
              <div className="h-3 w-14 bg-gray-300 rounded mb-2"/>
              <div className="h-3 w-[30rem] bg-gray-300 rounded mb-2"/>
            </div>
          </div>
          <div className="mt-8 flex w-full">
            <div className="w-2 h-2 rounded-full border border-yellow-500 mr-6"/>
            <div className="">
              <div className="h-3 w-24 bg-gray-300 rounded mb-2"/>
              <div className="h-3 w-80 bg-gray-300 rounded mb-2"/>
            </div>
          </div>
          <div className="mt-8 flex w-full">
            <div className="w-2 h-2 rounded-full border border-yellow-500 mr-6"/>
            <div className="">
              <div className="h-3 w-14 bg-gray-300 rounded mb-2"/>
              <div className="h-3 w-96 bg-gray-300 rounded mb-2"/>
            </div>
          </div>
          <div className="mt-8 flex w-full">
            <div className="w-2 h-2 rounded-full border border-yellow-500 mr-6"/>
            <div className="">
              <div className="h-3 w-32 bg-gray-300 rounded mb-2"/>
              <div className="h-3 w-[40rem] bg-gray-300 rounded mb-2"/>
            </div>
          </div>
          <div className="mt-8 flex w-full">
            <div className="w-2 h-2 rounded-full border border-yellow-500 mr-6"/>
            <div className="">
              <div className="h-3 w-14 bg-gray-300 rounded mb-2"/>
              <div className="h-3 w-40 bg-gray-300 rounded mb-2"/>
            </div>
          </div>
        </div>
        <div className="border border-stone-300 mb-6 px-6 py-7">
          <div className="w-full border-b border-b-stone-300 flex justify-between">
            <div className="h-4 bg-black w-20 rounded"/>
            <div className="h-6 bg-gray-300 w-6 mb-4 rounded"/>
          </div>
          <div className="w-full flex items-start justify-between pt-8 pb-5 border-b border-b-stone-300">
            <div className="">
              <div className="w-14 h-3 bg-gray-300 rounded"/>
              <div className="w-24 h-3 bg-gray-300 rounded mt-3 mb-4"/>
              <div className="w-10 h-4 bg-gray-300 rounded"/>
            </div>
            <div className="bg-sky-500 w-28 h-8 rounded"/>
          </div>
          <div className="w-full flex items-start justify-between pt-8 pb-5 border-b border-b-stone-300">
            <div className="">
              <div className="w-14 h-3 bg-gray-300 rounded"/>
              <div className="w-24 h-3 bg-gray-300 rounded mt-3 mb-4"/>
              <div className="w-10 h-4 bg-gray-300 rounded"/>
            </div>
            <div className="bg-sky-500 w-28 h-8 rounded"/>
          </div>
          <div className="w-full flex items-start justify-between pt-8 pb-5 border-b border-b-stone-300">
            <div className="">
              <div className="w-14 h-3 bg-gray-300 rounded"/>
              <div className="w-24 h-3 bg-gray-300 rounded mt-3 mb-4"/>
              <div className="w-10 h-4 bg-gray-300 rounded"/>
            </div>
            <div className="bg-sky-500 w-28 h-8 rounded"/>
          </div>
          <div className="w-full flex items-start justify-between pt-8 pb-5 border-b border-b-stone-300">
            <div className="">
              <div className="w-14 h-3 bg-gray-300 rounded"/>
              <div className="w-24 h-3 bg-gray-300 rounded mt-3 mb-4"/>
              <div className="w-10 h-4 bg-gray-300 rounded"/>
            </div>
            <div className="bg-sky-500 w-28 h-8 rounded"/>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Mint;