import type { NextPage } from "next";

const Airbnb: NextPage = () => (
  <div>
    <header className="mx-6">
      <div className="flex pt-4 justify-between">
        <div className="flex items-center">
          <div className="w-10 h-10 bg-rose-500 rounded-full" />
          <div className="border border-gray-200 ml-4">
            <div className="ml-4 h-4 w-32 bg-gray-300 mr-20 my-2" />
          </div>
          <div className="border border-gray-200">
            <div className="ml-4 h-4 w-32 bg-gray-300 mr-20 my-2" />
          </div>
          <div className="border border-gray-200">
            <div className="ml-4 h-4 w-32 bg-gray-300 mr-20 my-2" />
          </div>
        </div>
        <div className="flex items-center flex-row-reverse">
          <div className="w-8 h-8 bg-blue-800 rounded-full" />
          <div className="w-10 h-6 bg-gray-300 rounded-sm mx-10" />
          <div className="w-12 h-4 bg-gray-300 rounded-sm" />
          <div className="w-10 h-6 bg-gray-300 rounded-sm mx-10" />
          <div className="w-12 h-4 bg-gray-300 rounded-sm" />
        </div>
      </div>
      <div className="mt-4 -mx-6 px-6 border-b-4 border-gray-300 flex">
        <div className="h-6 w-14 bg-gray-500 rounded" />
        <div className="border-b-2 border-green-800 w-12 mx-5 h-8">
          <div className="h-6 w-full bg-green-800 rounded" />
        </div>
        <div className="h-6 w-20 bg-gray-500 rounded" />
      </div>
    </header>
    <div className="w-full h-full flex">
      <div className="w-4/6 pl-6 pt-6 mr-4">
        <div className="w-full flex justify-start">
          <div className="h-4 w-24 bg-gray-300 rounded" />
          <div className="h-4 w-24 bg-gray-300 rounded mx-6" />
          <div className="h-4 w-32 bg-gray-300 rounded" />
          <div className="h-4 w-28 bg-gray-300 rounded mx-6" />
          <div className="h-4 w-24 bg-gray-300 rounded" />
        </div>
        <div className="mt-8 h-4 bg-gray-300 w-8/12 rounded mb-12"/>
        <div className="w-full grid grid-cols-3 gap-x-4 gap-y-12">
          <div className="w-full rounded">
            <div className="h-48 bg-sky-300" />
            <div className=" rounded-sm mt-2 h-6 w-2/3 bg-gray-600" />
            <div className=" rounded-sm my-2 h-3 w-2/5 bg-green-800" /> 
            <div className=" rounded-sm h-3 w-4/5 bg-gray-300" /> 
          </div>
          <div className="w-full rounded">
            <div className="h-48 bg-purple-300" />
            <div className=" rounded-sm mt-2 h-6 w-2/3 bg-gray-600" />
            <div className=" rounded-sm my-2 h-3 w-2/5 bg-green-800" /> 
            <div className=" rounded-sm h-3 w-4/5 bg-gray-300" /> 
          </div>
          <div className="w-full rounded">
            <div className="h-48 bg-pink-300" />
            <div className=" rounded-sm mt-2 h-6 w-2/3 bg-gray-600" />
            <div className=" rounded-sm my-2 h-3 w-2/5 bg-green-800" /> 
            <div className=" rounded-sm h-3 w-4/5 bg-gray-300" /> 
          </div>
          <div className="w-full rounded">
            <div className="h-48 bg-rose-300" />
            <div className=" rounded-sm mt-2 h-6 w-2/3 bg-gray-600" />
            <div className=" rounded-sm my-2 h-3 w-2/5 bg-green-800" /> 
            <div className=" rounded-sm h-3 w-4/5 bg-gray-300" /> 
          </div>
          <div className="w-full rounded">
            <div className="h-48 bg-indigo-300" />
            <div className=" rounded-sm mt-2 h-6 w-2/3 bg-gray-600" />
            <div className=" rounded-sm my-2 h-3 w-2/5 bg-green-800" /> 
            <div className=" rounded-sm h-3 w-4/5 bg-gray-300" /> 
          </div>
          <div className="w-full rounded">
            <div className="h-48 bg-fuchsia-300" />
            <div className=" rounded-sm mt-2 h-6 w-2/3 bg-gray-600" />
            <div className=" rounded-sm my-2 h-3 w-2/5 bg-green-800" /> 
            <div className=" rounded-sm h-3 w-4/5 bg-gray-300" /> 
          </div>
          <div className="w-full rounded">
            <div className="h-48 bg-yellow-300" />
            <div className=" rounded-sm mt-2 h-6 w-2/3 bg-gray-600" />
            <div className=" rounded-sm my-2 h-3 w-2/5 bg-green-800" /> 
            <div className=" rounded-sm h-3 w-4/5 bg-gray-300" /> 
          </div>
          <div className="w-full rounded">
            <div className="h-48 bg-orange-300" />
            <div className=" rounded-sm mt-2 h-6 w-2/3 bg-gray-600" />
            <div className=" rounded-sm my-2 h-3 w-2/5 bg-green-800" /> 
            <div className=" rounded-sm h-3 w-4/5 bg-gray-300" /> 
          </div>
          <div className="w-full rounded">
            <div className="h-48 bg-blue-300" />
            <div className=" rounded-sm mt-2 h-6 w-2/3 bg-gray-600" />
            <div className=" rounded-sm my-2 h-3 w-2/5 bg-green-800" /> 
            <div className=" rounded-sm h-3 w-4/5 bg-gray-300" /> 
          </div>
        </div>
      </div>
      <div className="w-2/6 h-[42rem] bg-gray-200 sticky top-2 flex flex-col justify-between p-4">
        <div className="w-full flex items-start">
          <div className="w-7 h-14 bg-white rounded" />
          <div className="w-44 h-9 mx-4 bg-white rounded" />
        </div>
        <div className="w-full flex items-start flex-row-reverse">
          <div className="w-56 h-9 bg-white rounded" />
        </div>
      </div>
    </div>
  </div>
)

export default Airbnb;