import type { NextPage } from "next";

const Flights: NextPage = () => (
    <div>
      <header className="w-full flex justify-between py-3 h-auto sticky top-0 bg-white drop-shadow-xl">
        <div className="flex mx-4 items-center">
          <div className="w-8 h-4 bg-gray-400 rounded" />
          <div className="ml-4 w-16 h-5 bg-red-600 rounded"/>
        </div>
        <div className="flex mx-4">
          <div className="w-8 h-8 bg-gray-400 rounded" />
          <div className="w-8 h-8 bg-orange-400 rounded-full ml-4" />
        </div>
      </header>
      <div className="flex">
        <nav className="w-fit sticky top-16 h-fit">
          <div className="w-8 h-8 bg-gray-400 m-4 rounded" />
          <div className="w-8 h-8 bg-gray-400 m-4 rounded" />
          <div className="w-8 h-8 bg-gray-400 m-4 rounded" />
          <div className="w-8 h-8 bg-sky-400 m-4 rounded" />
          <div className="w-8 h-8 bg-gray-400 m-4 rounded" />
          <div className="w-8 h-8 bg-gray-400 m-4 rounded" />
        </nav>
        <div className="w-full flex flex-col items-center">
          <div className="w-3/5 my-4 bg-sky-200 h-96 flex flex-col justify-end items-center">
            <div className="w-1/5 h-14 bg-gray-50 mb-2" />
          </div>
          <div className="w-6/12 p-4 rounded-3xl border shadow-xl">
            <div className="flex w-fit mb-2
            ">
              <div className="w-14 h-4 bg-gray-100 rounded" />
              <div className="w-10 h-4 bg-gray-100 rounded mx-4" />
              <div className="w-14 h-4 bg-gray-100 rounded" />
            </div>
            <div className="w-full h-20 p-px flex items-center">
              <div className="w-3/5 h-full  inline-flex items-center">
                <div className="h-12 w-1/2 border border-black rounded mx-2" />
                <div className="h-12 w-1/2 border border-black rounded mx-2" />
              </div>
              <div className="w-2/5 h-12 mx-2 border rounded-md flex items-center">
                <div className="h-10 w-1/2 border border-r-black border-white " />
              </div>
            </div>
              <div className="w-24 h-10 bg-blue-600 rounded-full absolute left-2/4"/> 
          </div>
          <div className="w-6/12 h-24 border mt-10 bg-white rounded-xl drop-shadow-2xl"/>
          <div className="w-6/12 mt-6 mb-6">
            <div className="flex justify-between items-center">
              <div className="w-full inline-flex items-center mx-2">
                <div className="w-32 h-4 border bg-gray-200 rounded"/>
                <div className="w-14 h-2 mx-2 border bg-gray-200 rounded"/>
                <div className="w-10 h-2 border bg-gray-200 rounded"/>
              </div>
              <div className="w-24 h-4 bg-blue-600 rounded"/>
            </div>
            <div className="flex mt-2">
              <div className="w-1/2">
                <div className="w-full mb-2 border-2 bg-white rounded-xl flex">
                  <div className="w-1/2 bg-pink-400 rounded-l-xl"/>
                  <div className="w-1/2">
                    <div className="w-28 mx-2 mt-4 mb-1 h-4 bg-gray-400 rounded"/>
                    <div className="w-12 mx-2 mt-1 mb-1 h-4 bg-gray-400 rounded"/>
                    <div className="w-10 mx-2 mb-10 h-4 bg-gray-400 rounded"/>
                    <div className="w-10 mx-2 mb-10 h-4 bg-gray-400 rounded relative left-3/4 top-7"/>
                  </div>
                </div>
                <div className="w-full mb-2 border-2 bg-white rounded-xl flex">
                  <div className="w-1/2 bg-purple-400 rounded-l-xl"/>
                  <div className="w-1/2">
                    <div className="w-28 mx-2 mt-4 mb-1 h-4 bg-gray-400 rounded"/>
                    <div className="w-12 mx-2 mt-1 mb-1 h-4 bg-gray-400 rounded"/>
                    <div className="w-10 mx-2 mb-10 h-4 bg-gray-400 rounded"/>
                    <div className="w-10 mx-2 mb-10 h-4 bg-gray-400 rounded relative left-3/4 top-7"/>
                  </div>
                </div>
                <div className="w-full border-2 bg-white rounded-xl flex">
                  <div className="w-1/2 bg-green-400 rounded-l-xl"/>
                  <div className="w-1/2">
                    <div className="w-28 mx-2 mt-4 mb-1 h-4 bg-gray-400 rounded"/>
                    <div className="w-12 mx-2 mt-1 mb-1 h-4 bg-gray-400 rounded"/>
                    <div className="w-10 mx-2 mb-10 h-4 bg-gray-400 rounded"/>
                    <div className="w-10 mx-2 mb-10 h-4 bg-gray-400 rounded relative left-3/4 top-7"/>
                  </div>
                </div>
              </div>
              <div className="w-1/2 bg-sky-300 ml-2 rounded-2xl"></div>
            </div>
          </div>
          <hr className="w-1/2 mb-4 border-black" />
          <div className="w-1/2 flex justify-evenly items-center">
            <div className="bg-gray-200 w-28 h-8 rounded" />
            <div className="bg-gray-200 w-28 h-8 rounded" />
            <div className="bg-gray-200 w-28 h-8 rounded" />
          </div>
          <div className="w-1/2 my-4 flex justify-evenly items-center">
            <div className="bg-gray-200 w-28 h-2 rounded" />
            <div className="bg-gray-200 w-28 h-2 rounded" />
            <div className="bg-gray-200 w-28 h-2 rounded" />
            <div className="bg-gray-200 w-28 h-2 rounded" />
            <div className="bg-gray-200 w-28 h-2 rounded" />
            <div className="bg-gray-200 w-28 h-2 rounded" />
          </div>
          <div className="bg-gray-300 mb-2 w-1/2 h-4 rounded" />
        </div>
      </div>
    </div>
  )

export default Flights;