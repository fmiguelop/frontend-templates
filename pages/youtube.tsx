import type { NextPage } from 'next'
import Card from '../src/components/youtube/Card';

const Youtube: NextPage = () => {
  return (
  <div className='bg-gray-200'>
    <header className='bg-white flex items-center justify-between h-14'>
      <div className='inline-flex items-center'>
        <div className='w-6 h-6 bg-gray-200 ml-8 rounded-sm'/>
        <div className='w-28 h-10 bg-red-600 ml-2 rounded-sm'/>
      </div>
      <div className='w-2/4 h-8 bg-gray-200 ml-20 rounded-sm'/>
      <div className='inline-flex items-center'>
        <div className='w-6 h-6 bg-gray-200 ml-72 rounded-sm'/>
        <div className='w-6 h-6 bg-gray-200 ml-10 rounded-sm'/>
        <div className='w-9 h-9 bg-blue-900 mx-10 rounded-full'/>
      </div>
    </header>
    <div className='flex p-4 justify-center'>
      <div className='w-2/3 mr-4'>
        <div className="bg-black h-[39rem] flex items-end">
          <div className='bg-stone-800 w-full h-12'></div>
        </div>
        <div className='bg-white w-full rounded-md mt-4 h-44'/>
        <div className='bg-white w-full rounded-md mt-4 h-40'/>
        <div className='bg-white w-full rounded-md mt-4 h-40'/>
      </div>
      <aside className='h-screen sticky top-0'>
        <div className='bg-white w-96 '>
            <div className='w-full px-4 flex pt-8'>
              <div className="bg-green-400 w-1/2 h-24"/>
              <div className='w-1/2 pl-2'>
                <div className='bg-gray-600 w-full h-8 rounded-sm'/>
                <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
                <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
              </div>
            </div>
          <hr className='m-4 opacity-50' />
          <div className='w-full px-4 flex pt-4'>
              <div className="bg-sky-400 w-1/2 h-24"/>
              <div className='w-1/2 pl-2'>
                <div className='bg-gray-600 w-full h-8 rounded-sm'/>
                <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
                <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
              </div>
            </div>
          <div className='w-full px-4 flex pt-4'>
              <div className="bg-purple-400 w-1/2 h-24"/>
              <div className='w-1/2 pl-2'>
                <div className='bg-gray-600 w-full h-8 rounded-sm'/>
                <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
                <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
              </div>
            </div>
          <div className='w-full px-4 flex pt-4'>
              <div className="bg-pink-400 w-1/2 h-24"/>
              <div className='w-1/2 pl-2'>
                <div className='bg-gray-600 w-full h-8 rounded-sm'/>
                <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
                <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
              </div>
            </div>
          <div className='w-full px-4 flex pt-4'>
              <div className="bg-indigo-400 w-1/2 h-24"/>
              <div className='w-1/2 pl-2'>
                <div className='bg-gray-600 w-full h-8 rounded-sm'/>
                <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
                <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
              </div>
            </div>
          <div className='w-full px-4 flex pt-4'>
              <div className="bg-red-400 w-1/2 h-24"/>
              <div className='w-1/2 pl-2'>
                <div className='bg-gray-600 w-full h-8 rounded-sm'/>
                <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
                <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
              </div>
            </div>
          <div className='w-full px-4 flex pt-4'>
              <div className="bg-blue-400 w-1/2 h-24"/>
              <div className='w-1/2 pl-2'>
                <div className='bg-gray-600 w-full h-8 rounded-sm'/>
                <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
                <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
              </div>
            </div>
          <div className='w-full px-4 flex pt-4'>
              <div className="bg-violet-400 w-1/2 h-24"/>
              <div className='w-1/2 pl-2'>
                <div className='bg-gray-600 w-full h-8 rounded-sm'/>
                <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
                <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
              </div>
            </div>
            <div className='w-full px-4 flex pt-4'>
              <div className="bg-stone-400 w-1/2 h-24"/>
              <div className='w-1/2 pl-2'>
                <div className='bg-gray-600 w-full h-8 rounded-sm'/>
                <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
                <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
              </div>
            </div>
            <div className='w-full p-4 flex'>
              <div className="bg-rose-400 w-1/2 h-24"/>
              <div className='w-1/2 pl-2'>
                <div className='bg-gray-600 w-full h-8 rounded-sm'/>
                <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
                <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
              </div>
            </div>
        </div>
      </aside>
      
    </div>
    </div>  
  )
}

export default Youtube