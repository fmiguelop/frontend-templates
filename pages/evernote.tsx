import type { NextPage } from 'next'

const Evernote: NextPage = () => (
  <div className='flex'>
    <div className="p-4 bg-stone-200 w-fit h-fit sticky top-0">
      <div className="rounded-full w-8 h-8 bg-gray-500"/>
      <div className="rounded-full mt-8 w-8 h-8 bg-green-500"/>
      <div className="rounded-full mt-4 w-8 h-8 bg-green-500"/>
      <div className="rounded-full mt-4 w-8 h-8 bg-green-500"/>
      <div className="rounded-full mt-4 mb-14 w-8 h-8 bg-green-500"/>
      <div className="rounded-full mt-14 w-8 h-8 bg-green-500"/>
      <div className="rounded-full mt-4 w-8 h-8 bg-green-500"/>
      <div className="rounded-full mt-4 w-8 h-8 bg-green-500"/>
      <div className="rounded-full mt-4 mb-28 w-8 h-8 bg-green-500"/>
      <div className="rounded-full mt-4 w-8 h-8 bg-green-500"/>
      <hr className='h-0.5 mt-8 -mx-4 bg-gray-400' />
      <div className="rounded-full w-8 h-8 bg-gray-500 mt-4"/>
    </div>
    <div className="w-2/12 border-x h-fit border-gray-300 px-6 pt-6">
      <div className="bg-gray-200 h-6 w-20 rounded" />
      <div className="w-full flex justify-between mt-6 mb-2">
        <div className="w-16 h-4 bg-gray-200 rounded"/>
        <div className="w-16 h-4 bg-gray-200 rounded"/>
      </div>
      <hr className='h-px -mx-6 bg-gray-900'/>
      <div className="h-4 bg-gray-600 rounded w-40 mt-4" />
      <div className="h-4 bg-gray-300 rounded w-48 my-4" />
      <div className="h-4 border border-green-500 rounded-full w-full mb-4" />
      <div className="-mx-6 border-2 border-gray-400 px-6 py-4">
        <div className="bg-gray-500 h-4 w-32 rounded mb-1"/>
        <div className="bg-gray-500 h-3 w-28 rounded mb-4"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-1/2 rounded mb-1"/>
      </div>
      <div className="-mx-6 border border-gray-300 px-6 py-4">
        <div className="bg-gray-500 h-4 w-32 rounded mb-1"/>
        <div className="bg-gray-500 h-3 w-28 rounded mb-4"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-1/2 rounded mb-1"/>
      </div>
      <div className="-mx-6 border border-gray-300 px-6 py-4">
        <div className="bg-gray-500 h-4 w-32 rounded mb-1"/>
        <div className="bg-gray-500 h-3 w-28 rounded mb-4"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-1/2 rounded mb-1"/>
      </div>
      <div className="-mx-6 border border-gray-300 px-6 py-4">
        <div className="bg-gray-500 h-4 w-32 rounded mb-1"/>
        <div className="bg-gray-500 h-3 w-28 rounded mb-4"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-1/2 rounded mb-1"/>
      </div>
      <div className="-mx-6 border border-gray-300 px-6 py-4">
        <div className="bg-gray-500 h-4 w-32 rounded mb-1"/>
        <div className="bg-gray-500 h-3 w-28 rounded mb-4"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-1/2 rounded mb-1"/>
      </div>
      <div className="-mx-6 border border-gray-300 px-6 py-4">
        <div className="bg-gray-500 h-4 w-32 rounded mb-1"/>
        <div className="bg-gray-500 h-3 w-28 rounded mb-4"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-1/2 rounded mb-1"/>
      </div>
      <div className="-mx-6 border border-gray-300 px-6 py-4">
        <div className="bg-gray-500 h-4 w-32 rounded mb-1"/>
        <div className="bg-gray-500 h-3 w-28 rounded mb-4"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-full rounded mb-1"/>
        <div className="bg-gray-200 h-3 w-1/2 rounded mb-1"/>
      </div>
    </div>
    <div className="w-10/12">
      <header className="pl-6 pr-4 pt-5 flex justify-between">
        <div className='flex'>
          <div className="h-6 w-6 bg-gray-200 rounded" />
          <div className="h-6 w-6 bg-gray-200 rounded mx-4" />
          <div className="h-6 w-6 bg-gray-200 rounded" />
          <div className="h-6 w-6 bg-gray-200 rounded mx-4" />
          <div className="h-6 w-6 bg-gray-200 rounded" />
        </div>
        <div className="flex">
          <div className="h-7 w-7 bg-gray-200 rounded" />
          <div className="h-7 w-24 border-2 border-gray-500 rounded mx-2" />
          <div className="h-7 w-7 border-2 border-green-500 rounded" />
        </div>
      </header>
      <div className='flex justify-between items-center pt-4 pb-3 border-b border-gray-300 ml-14 mr-24'>
        <div className='flex'>
          <div className="w-4 h-4 bg-gray-200 rounded"/>
          <div className="w-px h-5 bg-gray-200 mx-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded"/>
          <div className="w-px h-5 bg-gray-200 mx-3"/>
          <div className="w-11 h-4 bg-gray-200 rounded "/>
        </div>
        <div className='flex'>
          <div className="w-px h-5 bg-gray-200 mr-3"/>
          <div className="w-11 h-4 bg-gray-200 rounded"/>
          <div className="w-px h-5 bg-gray-200 mx-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded"/>
          <div className="w-px h-5 bg-gray-200 mx-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded"/>
          <div className="w-px h-5 bg-gray-200 mx-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded"/>
          <div className="w-px h-5 bg-gray-200 mx-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded"/>
          <div className="w-px h-5 bg-gray-200 mx-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded mr-3"/>
          <div className="w-4 h-4 bg-gray-200 rounded"/>
          <div className="w-px h-5 bg-gray-200 mx-3"/>
          <div className="w-14 h-4 bg-gray-200 rounded mr-16"/>
        </div>
      </div>
      <div className="ml-14 mt-4">
        <div className="w-1/5 h-7 bg-green-500 rounded"/>
        <div className="w-4/5 h-5 mt-10 bg-gray-300 rounded"/>
        <div className="w-2/5 h-5 bg-gray-300 rounded mt-2"/>
      </div>
    </div>
  </div>
)

export default Evernote;