import React, { FC } from 'react'

interface Props {
  color: string
}

const Card: FC<Props> = ({ color }) => {
  return (
  <div className='w-full px-4 flex pt-8'>
    <div className={`bg-${color}-400 w-1/2 h-24`}/>
    <div className='w-1/2 pl-2'>
      <div className='bg-gray-600 w-full h-8 rounded-sm'/>
      <div className='bg-gray-200 w-3/5 my-2 h-2 rounded-sm'/>
      <div className='bg-gray-200 w-2/5 h-2 rounded-sm'/>
    </div>
  </div>
)
}

export default Card