import React, { FC } from 'react'

interface Props {
  children: React.ReactNode
}
const Card:FC<Props> = ({ children }) => (
  <div className="bg-gray-200 rounded-md p-2 ml-2 h-fit">
    <div className="w-20 h-4 mb-2 bg-gray-400 rounded-md"/>
    { children }
    <div className="w-20 h-4 my-4 bg-white rounded-md"/>
  </div>
)

export default Card